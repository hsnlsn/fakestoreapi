//
//  UserEndpoint.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public enum UserEndpoint {
    case getAllUsers(urlParameters: Parameters?)
    case getSingleUser(pathParameters: Parameters?)
    case addUser(bodyParameters: Parameters?)
    case updateUser(pathParameters: Parameters?, bodyParameters: Parameters?)
    case deleteUser(pathParameters: Parameters?)
}

extension UserEndpoint: HEndpointType {
    var path: String {
        switch self {
        case .getAllUsers,
                .addUser:
            return "/users"
        case .getSingleUser,
                .updateUser,
                .deleteUser:
            return "/users/{{userId}}"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getAllUsers,
                .getSingleUser:
            return .get
        case .addUser:
            return .post
        case .updateUser:
            return .put
        case .deleteUser:
            return .delete
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .getAllUsers(let urlParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                urlParameters: urlParameters)
        case .getSingleUser(let pathParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                pathParameters: pathParameters)
        case .addUser(let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters)
        case .updateUser(let pathParameters, let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters,
                                                pathParameters: pathParameters)
        case .deleteUser(let pathParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                pathParameters: pathParameters)
        }
    }
}
