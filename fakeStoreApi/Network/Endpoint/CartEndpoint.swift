//
//  CartEndpoint.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public enum CartEndpoint {
    case getAllCarts(urlParameters: Parameters?)
    case getSingleCart(pathParameters: Parameters?)
    case getUserCarts(pathParameters: Parameters?)
    case addCart(bodyParameters: Parameters?)
    case updateCart(pathParameters: Parameters?, bodyParameters: Parameters?)
    case deleteCart(pathParameters: Parameters?)
}

extension CartEndpoint: HEndpointType {
    var path: String {
        switch self {
        case .getAllCarts,
                .addCart:
            return "/carts"
        case .getSingleCart,
                .updateCart,
                .deleteCart:
            return "/carts/{{cartId}}"
        case .getUserCarts:
            return "/carts/user/{{userId}}"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getAllCarts,
                .getSingleCart,
                .getUserCarts:
            return .get
        case .addCart:
            return .post
        case .updateCart:
            return .put
        case .deleteCart:
            return .delete
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .getAllCarts(let urlParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                urlParameters: urlParameters)
        case .getSingleCart(let pathParameters),
                .getUserCarts(let pathParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                pathParameters: pathParameters)
        case .addCart(let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters)
        case .updateCart(let pathParameters, let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters,
                                                pathParameters: pathParameters)
        case .deleteCart(let pathParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                pathParameters: pathParameters)
        }
    }
}
