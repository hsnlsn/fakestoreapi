//
//  AuthEndpoint.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public enum AuthEndpoint {
    case login(bodyParameters: Parameters?)
}

extension AuthEndpoint: HEndpointType {
    var path: String {
        switch self {
        case .login:
            return "/auth/login"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .login(let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters)
        }
    }
}
