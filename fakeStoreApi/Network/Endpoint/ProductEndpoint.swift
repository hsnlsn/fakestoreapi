//
//  ProductEndpoint.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public enum ProductEndpoint {
    case getAllProducts(urlParameters: Parameters?)
    case getSingleProduct(pathParameters: Parameters?)
    case getCategoryProduct(pathParameters: Parameters?)
    case addProduct(bodyParameters: Parameters?)
    case updateProduct(pathParameters: Parameters?, bodyParameters: Parameters?)
    case deleteProduct(pathParameters: Parameters?)
}

extension ProductEndpoint: HEndpointType {
    var path: String {
        switch self {
        case .getAllProducts,
                .addProduct:
            return "/products"
        case .getSingleProduct,
                .updateProduct,
                .deleteProduct:
            return "/products/{{productId}}"
        case .getCategoryProduct:
            return "/products/category/{{categoryName}}"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getAllProducts,
                .getSingleProduct,
                .getCategoryProduct:
            return .get
        case .addProduct:
            return .post
        case .updateProduct:
            return .put
        case .deleteProduct:
            return .delete
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .getAllProducts(let urlParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                urlParameters: urlParameters)
        case .getSingleProduct(let pathParameters),
                .getCategoryProduct(let pathParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                pathParameters: pathParameters)
        case .addProduct(let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters)
        case .updateProduct(let pathParameters, let bodyParameters):
            return .requestParametersAndHeaders(contentType: .json,
                                                bodyParameters: bodyParameters,
                                                pathParameters: pathParameters)
        case .deleteProduct(let pathParameters):
            return .requestParametersAndHeaders(contentType: .urlEncoded,
                                                pathParameters: pathParameters)
        }
    }
}
