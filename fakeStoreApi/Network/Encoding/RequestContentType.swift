//
//  RequestContentType.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

extension Parameters: RequestParameters { }
extension ParametersArray: RequestParameters { }

public protocol RequestParameters { }

public typealias Parameters = [String: Any]
public typealias ParametersArray = [[String: Any]]
public typealias MultipartParameterData = [String: Data]

public protocol ParameterEncoder {
  func encode(urlRequest: inout URLRequest, with parameters: RequestParameters) throws
}

public indirect enum RequestContentType {
  
  case urlEncoded
  case json
  case xml
  case textPlain
  case textHtml
  case javascript
  case none
  
  public func configure(urlRequest: inout URLRequest,
                        bodyParameters: RequestParameters?,
                        urlParameters: RequestParameters?,
                        pathParameters: RequestParameters?,
                        formParameters: RequestParameters?) throws {
    do {
      if let pathParameters = pathParameters {
        try PathParameterEncoding().encode(urlRequest: &urlRequest, with: pathParameters)
      }
      
      if let urlParameters = urlParameters {
        try URLParameterEncoder().encode(urlRequest: &urlRequest, with: urlParameters)
      }
      
      if let bodyParameters = bodyParameters {
        try JSONParameterEncoding().encode(urlRequest: &urlRequest, with: bodyParameters)
      }
      
      if let formParameters = formParameters {
        try FormUrlParameterEncoding().encode(urlRequest: &urlRequest, with: formParameters)
      }
      
      switch self {
      case .urlEncoded:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        }
      case .json:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
      case .xml:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("application/xml", forHTTPHeaderField: "Content-Type")
        }
      case .textPlain:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("text/plain", forHTTPHeaderField: "Content-Type")
        }
      case .textHtml:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("text/html", forHTTPHeaderField: "Content-Type")
        }
      case .javascript:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("application/javascript", forHTTPHeaderField: "Content-Type")
        }
      default:
        break
      }
    } catch {
      throw error
    }
  }
}

public enum NetworkError : String, Error {
  case parametersNil = "Parameters were nil."
  case encodingFailed = "Parameter encoding failed."
  case missingURL = "URL is nil."
}

