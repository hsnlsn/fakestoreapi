//
//  JSONParameterEncoding.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public struct JSONParameterEncoding: ParameterEncoder {
  public func encode(urlRequest: inout URLRequest, with parameters: RequestParameters) throws {
    do {
      let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
      urlRequest.httpBody = jsonAsData
    }catch {
      throw NetworkError.encodingFailed
    }
  }
}

