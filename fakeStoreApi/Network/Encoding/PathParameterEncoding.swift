//
//  PathParameterEncoding.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public struct PathParameterEncoding: ParameterEncoder {
  public func encode(urlRequest: inout URLRequest, with parameters: RequestParameters) throws {
    guard let url = urlRequest.url else { throw NetworkError.missingURL }
    
    if let decodedUrlStr = url.absoluteString.removingPercentEncoding {
      let newUrlStr = decodedUrlStr.replacePathParameters(pathParameters: parameters)
      
      if let encodedStr = newUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
        urlRequest.url = URL(string: encodedStr)
      }
    }
  }
}

fileprivate extension String {
  func replacePathParameters(pathParameters: RequestParameters?) -> String {
    var pathString = self
    if let pathParametersObject = pathParameters as? [String: Any] {
      pathParametersObject.keys.forEach({ (key) in
        if let val = pathParametersObject[key] as? String {
          pathString = pathString.replacingOccurrences(of: "{{\(key)}}", with: val)
        }
      })
    }
    
    return pathString
  }
}
