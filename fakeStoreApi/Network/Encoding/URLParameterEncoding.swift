//
//  URLEncoding.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public struct URLParameterEncoder: ParameterEncoder {
  public func encode(urlRequest: inout URLRequest, with parameters: RequestParameters) throws {
    guard let url = urlRequest.url else { throw NetworkError.missingURL }
    if var urlComponents = URLComponents(url: url,
                                         resolvingAgainstBaseURL: false),
       let parametersObject = parameters as? [String: Any],
       !parametersObject.isEmpty {
      
      if urlComponents.queryItems == nil {
        urlComponents.queryItems = [URLQueryItem]()
      }
      
      for (key, value) in parametersObject {
        let queryItem = URLQueryItem(name: key, value: "\(value)")
        urlComponents.queryItems?.append(queryItem)
      }
      
      urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?
        .replacingOccurrences(of: "+", with: "%2B")
        .replacingOccurrences(of: "/", with: "%2F")
      
      urlRequest.url = urlComponents.url
    }
  }
}
