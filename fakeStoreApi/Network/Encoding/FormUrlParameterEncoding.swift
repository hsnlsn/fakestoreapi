//
//  FormUrlParameterEncoding.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public struct FormUrlParameterEncoding: ParameterEncoder {
  public func encode(urlRequest: inout URLRequest, with parameters: RequestParameters) throws {
    guard let parametersObject = parameters as? [String: Any] else {
      return
    }
    
    var httpBodyString: String = ""
    httpBodyString = parametersObject.map { "\($0)=\($1)" }.joined(separator: "&")
    
    urlRequest.httpBody = httpBodyString.data(using: .utf8)
  }
}
