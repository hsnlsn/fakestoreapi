//
//  EndPoint.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import Alamofire

protocol HEndpointType {
  var baseURL: URL { get }
  var path: String { get }
  var httpMethod: HTTPMethod { get }
  var task: HTTPTask { get }
  var cachePolicy: NSURLRequest.CachePolicy { get }
  var timeoutInterval: TimeInterval { get }
}

extension HEndpointType {
  var baseURL: URL {
    guard let url = URL(string: HConstants.API_HOST_URL) else {
      fatalError("baseURL could not be configured.")
    }
    
    return url
  }

  var timeoutInterval: TimeInterval {
    return 200
  }
  
  var cachePolicy: NSURLRequest.CachePolicy {
    return .reloadIgnoringLocalCacheData
  }
}

