//
//  HNetworkManager.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import Alamofire
import ObjectMapper

enum HNetworkResult<T> {
    case success(T)
    case successArray([T])
    case failure(HNetworkError)
}

typealias RequestCompletionBlock<T> = ((HNetworkResult<T>) -> Void)?

struct HNetworkManager {
    static let shared = HNetworkManager()
    
    func sendRequest<T: Mappable>(with endpoint: HEndpointType, completion: RequestCompletionBlock<T>) {
        do {
            let request = try self.buildRequest(from: endpoint)
            
            AF.request(request)
                .validate()
                .responseJSON { responseJson in
                    switch responseJson.result {
                    case .success(let value):
                        if let val = value as? [[String: Any]] {
                            let result = Mapper<T>().mapArray(JSONArray: val)
                            completion?(.successArray(result))
                            
                            return
                        }
                        
                        if let val = value as? [String: Any],
                           let result = Mapper<T>().map(JSON: val) {
                            completion?(.success(result))
                            
                            return
                        }
                        
                        completion?(.failure(.decodingFailed))
                    case .failure(let error):
                        let err = HNetworkError(code: error.responseCode)
                        completion?(.failure(err))
                    }
                }
        } catch {
            completion?(.failure(.badRequest))
        }
    }
}

extension HNetworkManager {
    fileprivate func buildRequest(from endpoint: HEndpointType) throws -> URLRequest {
        var request = URLRequest(url: endpoint.baseURL.appendingPathComponent(endpoint.path),
                                 cachePolicy: endpoint.cachePolicy,
                                 timeoutInterval: endpoint.timeoutInterval)
        request.httpMethod = endpoint.httpMethod.rawValue
        
        do {
            switch endpoint.task {
            case .requestParametersAndHeaders(let contentType,
                                              let bodyParameters,
                                              let urlParameters,
                                              let pathParameters,
                                              let formParameters,
                                              let additionalHeaders):
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                
                try contentType.configure(urlRequest: &request,
                                          bodyParameters: bodyParameters,
                                          urlParameters: urlParameters,
                                          pathParameters: pathParameters,
                                          formParameters: formParameters)
            }
            
            return request
        } catch {
            throw error
        }
    }
    
    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
