//
//  ApiError.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

enum HNetworkError: Error {
  case failed
  case badRequest
  case forbidden
  case notFound
  case decodingFailed
  case unauthorized
  case custom(message: String?)
  
  init(code: Int?) {
    switch code {
    case 400:
      self = .badRequest
    case 401:
      self = .unauthorized
    case 403:
      self = .forbidden
    case 404:
      self = .notFound
    default:
      self = .failed
    }
  }
}
