//
//  HTTPTask.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum HTTPTask {
  case requestParametersAndHeaders(contentType: RequestContentType,
                                   bodyParameters: RequestParameters? = nil,
                                   urlParameters: RequestParameters? = nil,
                                   pathParameters: RequestParameters? = nil,
                                   formParameters: RequestParameters? = nil,
                                   additionalHeaders: HTTPHeaders? = nil)
}
