//
//  ProductListViewController.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import UIKit

class ProductListViewController: BaseViewController {
    
    // MARK: - Private Properties -
    
    private lazy var productListTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ProductCell.self, forCellReuseIdentifier: ProductCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    private var viewModel: (ProductListViewToViewModelProtocol & ProductListDataSourceProtocol)!
    
    //
    
    // MARK: - Object lifecycle -
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: (ProductListViewToViewModelProtocol & ProductListDataSourceProtocol)) {
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    // MARK: - View lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareViews()
        
        self.configureViews()
        
        self.viewModel.viewModelDelegate = self
        self.viewModel.getAllProducts()
    }
    
    //
    
    // MARK: - Helpers -
    
    private func prepareViews() {
        self.view.addSubview(self.productListTableView)
        self.productListTableView.snp.makeConstraints { make in
            make.trailing.leading.top.bottom.equalToSuperview()
        }
    }
    
    private func configureViews() {
        self.title = "Products"
    }
    
    //
}

extension ProductListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.productCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductCell.reuseIdentifier) as? ProductCell else {
            return UITableViewCell()
        }
        
        let item = self.viewModel.productItem(at: indexPath)
        cell.configure(item: item)
        return cell
    }
}

extension ProductListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.viewModel.productItem(at: indexPath)
        let viewModel = ProductDetailViewModel(productModel: item)
        let viewController = ProductDetailViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ProductListViewController: ProductListViewModelToViewDelegate {
    func showProductList() {
        self.productListTableView.reloadData()
    }
}
