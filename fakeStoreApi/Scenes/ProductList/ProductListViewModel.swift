//
//  ProductListViewModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import ObjectMapper

protocol ProductListDataSourceProtocol: AnyObject {
    func productCount() -> Int
    func productItem(at indexPath: IndexPath) -> ProductModel
}

protocol ProductListViewToViewModelProtocol: AnyObject {
    var viewModelDelegate: ProductListViewModelToViewDelegate? { get set }
    
    func getAllProducts()
}

protocol ProductListViewModelToViewDelegate: AnyObject {
    func showProductList()
}

class ProductListViewModel {
    // MARK: - Public Properties -
    
    weak var delegate: ProductListViewModelToViewDelegate?
    
    //
    
    // MARK: - Private Properties -
    
    private var productList: [ProductModel] = []
    
    //
}

extension ProductListViewModel: ProductListDataSourceProtocol {
    func productCount() -> Int {
        return self.productList.count
    }
    
    func productItem(at indexPath: IndexPath) -> ProductModel {
        return self.productList[indexPath.row]
    }
}

extension ProductListViewModel: ProductListViewToViewModelProtocol {
    var viewModelDelegate: ProductListViewModelToViewDelegate? {
        get {
            return self.delegate
        }
        set {
            self.delegate = newValue
        }
    }
    
    func getAllProducts() {
        let urlParameters: [String: Any] = ["limit": 20, "sort": "desc"]
        
        HNetworkManager.shared.sendRequest(with: ProductEndpoint.getAllProducts(urlParameters: urlParameters))
        { [weak self] (result: HNetworkResult<ProductModel>) in
            switch result {
            case .successArray(let productList):
                self?.productList = productList
                
                self?.delegate?.showProductList()
            case .failure(let error):
                print(error.localizedDescription)
            default:
                break
            }
        }
    }
}
