//
//  ProductModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import ObjectMapper

struct ProductModel: Mappable {
    var id: Int?
    var title: String?
    var price: Double?
    var desc: String?
    var category: String?
    var image: URL?
    var rating: ProductRatingModel?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.id <- map["id"]
        self.title <- map["title"]
        self.price <- map["price"]
        self.desc <- map["description"]
        self.category <- map["category"]
        self.image <- (map["image"], URLTransform())
        self.rating <- map["rating"]
    }
}

struct ProductRatingModel: Mappable {
    var rate: Double?
    var count: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.rate <- map["rate"]
        self.count <- map["count"]
    }
}
