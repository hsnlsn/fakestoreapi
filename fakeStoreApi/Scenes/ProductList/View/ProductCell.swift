//
//  ProductCell.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import UIKit
import Kingfisher

class ProductCell: UITableViewCell {
    static let reuseIdentifier = "ProductCell"
    
    // MARK: - Private Properties -
    
    private lazy var innerContainerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 16
        view.layer.borderColor = UIColor.systemGroupedBackground.cgColor
        view.layer.borderWidth = 1
        view.clipsToBounds = true
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 16
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.systemGroupedBackground.cgColor
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .lightGray
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    private lazy var cartButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add To Cart", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.backgroundColor = .purple
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        return button
    }()
    
    //
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        self.contentView.addSubview(self.innerContainerView)
        self.innerContainerView.addSubview(self.productImageView)
        self.innerContainerView.addSubview(self.titleLabel)
        self.innerContainerView.addSubview(self.categoryLabel)
        self.innerContainerView.addSubview(self.priceLabel)
        self.innerContainerView.addSubview(self.cartButton)
        
        self.innerContainerView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
        }
        
        self.productImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().offset(-12)
            make.width.equalTo(100)
            make.height.equalTo(100)
        }
        
        self.titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.productImageView.snp.trailing).offset(12)
            make.trailing.equalToSuperview().offset(-12)
            make.top.equalTo(self.productImageView.snp.top)
        }
        
        self.categoryLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.titleLabel.snp.leading)
            make.trailing.equalTo(self.titleLabel.snp.trailing)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(4)
        }
        
        self.priceLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.categoryLabel.snp.leading)
            make.top.greaterThanOrEqualTo(self.categoryLabel.snp.bottom).offset(4)
            make.bottom.equalTo(self.productImageView.snp.bottom)
        }
        
        self.cartButton.snp.makeConstraints { make in
            make.trailing.equalTo(self.categoryLabel.snp.trailing)
            make.leading.greaterThanOrEqualTo(self.priceLabel.snp.trailing).offset(4)
            make.bottom.equalTo(self.priceLabel.snp.bottom)
            make.height.equalTo(24)
        }
    }
    
    func configure(item: ProductModel) {
        self.titleLabel.text = item.title
        self.categoryLabel.text = item.category
        self.priceLabel.text = "$\(item.price ?? 0)"
        self.productImageView.kf.setImage(with: item.image)
    }
}
