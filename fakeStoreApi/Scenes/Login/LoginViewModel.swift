//
//  LoginViewModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

protocol LoginViewToViewModelProtocol: AnyObject {
    var viewModelDelegate: LoginViewModelToViewDelegate? { get set }
    
    func loadUser()
    func loginUser(username: String?, password: String?)
}

protocol LoginViewModelToViewDelegate: AnyObject {
    func routeToHome()
    func fillUserInfo(username: String?, password: String?)
}

class LoginViewModel {
    // MARK: - Public Properties -
    
    weak var delegate: LoginViewModelToViewDelegate?
    
    //
    
    // MARK: - Private Properties -
    
    private var userModel: UserModel!
    
    //
    
    init(userModel: UserModel) {
        self.userModel = userModel
    }
}

extension LoginViewModel: LoginViewToViewModelProtocol {
    var viewModelDelegate: LoginViewModelToViewDelegate? {
        get {
            return self.delegate
        }
        set {
            self.delegate = newValue
        }
    }
    
    func loadUser() {
        self.delegate?.fillUserInfo(username: self.userModel.username,
                                    password: self.userModel.password)
    }
    
    func loginUser(username: String?, password: String?) {
        guard let username = username,
              let password = password else {
            return
        }
        
        let bodyParameters: [String: Any] = ["username": username, "password": password]
        
        HNetworkManager.shared.sendRequest(with: AuthEndpoint.login(bodyParameters: bodyParameters))
        { [weak self] (result: HNetworkResult<AccessTokenModel>) in
            switch result {
            case .success(let accessTokenModel):
                HAuthorizationManager.shared.saveAccessToken(accessTokenModel: accessTokenModel)
                
                if let userModel = self?.userModel {
                    HAuthorizationManager.shared.saveUserProfile(userModel: userModel)
                }
                
                self?.delegate?.routeToHome()
            case .failure(let error):
                print(error.localizedDescription)
            default:
                break
            }
        }
    }
}
