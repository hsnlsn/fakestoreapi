//
//  LoginViewController.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import UIKit

class LoginViewController: BaseViewController {
    
    // MARK: - Private Properties -
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var usernameField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "username"
        textField.textAlignment = .center
        textField.backgroundColor = .systemGroupedBackground
        textField.layer.cornerRadius = 6
        textField.clipsToBounds = true
        return textField
    }()
    
    private lazy var passwordField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "password"
        textField.textAlignment = .center
        textField.isSecureTextEntry = true
        textField.backgroundColor = .systemGroupedBackground
        textField.layer.cornerRadius = 6
        textField.clipsToBounds = true
        return textField
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("LOGIN", for: .normal)
        button.addTarget(self, action: #selector(self.loginAction(_:)), for: .touchUpInside)
        button.layer.cornerRadius = 6
        button.clipsToBounds = true
        button.backgroundColor = .purple
        return button
    }()
    
    private var viewModel: LoginViewModel!
    
    //
    
    // MARK: - Object lifecycle -
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: LoginViewModel) {
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    //
    
    // MARK: - View lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareViews()
        
        self.configureViews()
        
        self.viewModel.viewModelDelegate = self
        self.viewModel.loadUser()
    }
    
    //
    
    // MARK: - Helpers -
    
    private func prepareViews() {
        self.view.addSubview(self.containerStackView)
        self.containerStackView.addArrangedSubview(self.usernameField)
        self.containerStackView.addArrangedSubview(self.passwordField)
        self.containerStackView.addArrangedSubview(self.loginButton)
        
        self.containerStackView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(16)
        }
        
        self.loginButton.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
    }
    
    private func configureViews() {
        self.title = "Login"
    }
    
    //
    
    // MARK: - Actions -
    
    @objc private func loginAction(_ sender: UIButton) {
        self.viewModel.loginUser(username: self.usernameField.text,
                                 password: self.passwordField.text)
    }
    
    //
}

extension LoginViewController: LoginViewModelToViewDelegate {
    func routeToHome() {
        if let sceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate {
            let tabBarController = MainTabBarViewController()
            sceneDelegate.window?.rootViewController = tabBarController
            sceneDelegate.window?.makeKeyAndVisible()
        }
    }
    
    func fillUserInfo(username: String?, password: String?) {
        self.usernameField.text = username
        self.passwordField.text = password
    }
}
