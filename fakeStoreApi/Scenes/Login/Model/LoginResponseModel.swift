//
//  AccessTokenModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import ObjectMapper

struct AccessTokenModel: Mappable {
    var token: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.token <- map["token"]
    }
}
