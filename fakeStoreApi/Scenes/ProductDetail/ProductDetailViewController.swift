//
//  ProductDetailViewController.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 3.01.2023.
//

import UIKit

class ProductDetailViewController: BaseViewController {
    // MARK: - Private Properties -
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        return scrollView
    }()
    
    private lazy var innerContainerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .lightGray
        return label
    }()
    
    private lazy var ratingContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.borderColor = UIColor.systemGroupedBackground.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 18
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var ratingIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star.fill")
        imageView.tintColor = .orange
        return imageView
    }()
    
    private lazy var ratingLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var bottomContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGroupedBackground
        return view
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 24)
        return label
    }()
    
    private lazy var cartButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add To Cart", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.backgroundColor = .purple
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        return button
    }()
    
    private var viewModel: ProductDetailViewToViewModelProtocol!
    
    //
    
    // MARK: - Object lifecycle -
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: ProductDetailViewToViewModelProtocol) {
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    // MARK: - View lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareViews()
        
        self.configureViews()
        
        self.viewModel.viewModelDelegate = self
        self.viewModel.getProductDetail()
    }
    
    //
    
    // MARK: - Helpers -
    
    private func prepareViews() {
        self.view.addSubview(self.scrollView)
        self.view.addSubview(self.bottomContainerView)
        
        self.scrollView.addSubview(self.innerContainerView)
        self.innerContainerView.addSubview(self.productImageView)
        self.innerContainerView.addSubview(self.titleLabel)
        self.innerContainerView.addSubview(self.descLabel)
        self.innerContainerView.addSubview(self.categoryLabel)
        self.innerContainerView.addSubview(self.ratingContainerView)
        
        self.ratingContainerView.addSubview(self.ratingLabel)
        self.ratingContainerView.addSubview(self.ratingIconImageView)
        
        self.bottomContainerView.addSubview(self.priceLabel)
        self.bottomContainerView.addSubview(self.cartButton)
        
        self.scrollView.snp.makeConstraints { make in
            make.trailing.leading.top.equalToSuperview()
        }
        
        self.innerContainerView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.view.snp.height).priority(.low)
        }
        
        self.productImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(self.productImageView.snp.width).multipliedBy(1)
        }
        
        self.titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalTo(self.productImageView.snp.bottom).offset(20)
        }
        
        self.categoryLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.titleLabel.snp.leading)
            make.trailing.equalTo(self.titleLabel.snp.trailing)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(4)
        }
        
        self.ratingContainerView.snp.makeConstraints { make in
            make.leading.greaterThanOrEqualTo(self.titleLabel.snp.trailing).offset(20)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalTo(self.titleLabel.snp.top)
        }
        
        self.ratingIconImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(12)
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
            make.width.height.equalTo(20)
        }
        
        self.ratingLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.ratingIconImageView.snp.trailing).offset(6)
            make.trailing.equalToSuperview().offset(-12)
            make.centerY.equalToSuperview()
        }
        
        self.descLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.categoryLabel.snp.leading)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalTo(self.categoryLabel.snp.bottom).offset(16)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        self.bottomContainerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.scrollView.snp.bottom)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
        
        self.priceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        self.cartButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.leading.greaterThanOrEqualTo(self.priceLabel.snp.trailing).offset(4)
            make.centerY.equalToSuperview()
            make.height.equalTo(50)
        }
    }
    
    private func configureViews() {
        self.title = "Product Detail"
    }
    
    //
}

extension ProductDetailViewController: ProductDetailViewModelToViewDelegate {
    func showProductDetail(productModel: ProductModel) {
        self.titleLabel.text = productModel.title
        self.categoryLabel.text = productModel.category
        self.priceLabel.text = "$\(productModel.price ?? 0)"
        self.ratingLabel.text = "\(productModel.rating?.rate ?? 0.0)"
        self.productImageView.kf.setImage(with: productModel.image)
        
        self.descLabel.attributedText = productModel.desc?.addingParagraphStyle(lineSpacing: 1.4)
    }
}
