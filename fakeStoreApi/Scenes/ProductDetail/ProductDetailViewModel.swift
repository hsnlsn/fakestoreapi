//
//  ProductDetailViewModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 3.01.2023.
//

import Foundation
import ObjectMapper

protocol ProductDetailViewToViewModelProtocol: AnyObject {
    var viewModelDelegate: ProductDetailViewModelToViewDelegate? { get set }
    
    func getProductDetail()
}

protocol ProductDetailViewModelToViewDelegate: AnyObject {
    func showProductDetail(productModel: ProductModel)
}

class ProductDetailViewModel {
    // MARK: - Public Properties -
    
    weak var delegate: ProductDetailViewModelToViewDelegate?
    
    //
    
    // MARK: - Private Properties -
    
    private var productModel: ProductModel!
    
    //
    
    init(productModel: ProductModel) {
        self.productModel = productModel
    }
}

extension ProductDetailViewModel: ProductDetailViewToViewModelProtocol {
    var viewModelDelegate: ProductDetailViewModelToViewDelegate? {
        get {
            return self.delegate
        }
        set {
            self.delegate = newValue
        }
    }
    
    func getProductDetail() {
        guard let productId = self.productModel.id else {
            return
        }
        
        let pathParameters: [String: Any] = ["productId": "\(productId)"]
        HNetworkManager.shared.sendRequest(with: ProductEndpoint.getSingleProduct(pathParameters: pathParameters))
        { [weak self] (result: HNetworkResult<ProductModel>) in
            switch result {
            case .success(let productModel):
                self?.productModel = productModel
                
                self?.delegate?.showProductDetail(productModel: productModel)
            case .failure(let error):
                print(error.localizedDescription)
            default:
                break
            }
        }
    }
}
