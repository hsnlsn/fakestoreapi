//
//  CartModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import ObjectMapper

struct CartModel: Mappable {
    var id: Int?
    var userId: Int?
    var date: Date?
    var products: [CartProductModel] = []
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.id <- map["id"]
        self.userId <- map["userId"]
        self.date <- map["date"]
        self.products <- map["products"]
    }
}

struct CartProductModel: Mappable {
    var productId: Int?
    var quantity: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.productId <- map["productId"]
        self.quantity <- map["quantity"]
    }
}
