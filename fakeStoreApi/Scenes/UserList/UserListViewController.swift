//
//  UserListViewController.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import UIKit
import SnapKit

class UserListViewController: BaseViewController {
    
    // MARK: - Private Properties -
    
    private lazy var userListTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UserListItemCell.self, forCellReuseIdentifier: UserListItemCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    private var viewModel: (UserListViewToViewModelProtocol & UserListDataSourceProtocol)!
    
    //
    
    // MARK: - Object lifecycle -
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: (UserListViewToViewModelProtocol & UserListDataSourceProtocol)) {
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    //
    
    // MARK: - View lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareViews()
        
        self.configureViews()
        
        self.viewModel.viewModelDelegate = self
        self.viewModel.getAllUsers()
    }
    
    //
    
    // MARK: - Helpers -
    
    private func prepareViews() {
        self.view.addSubview(self.userListTableView)
        self.userListTableView.snp.makeConstraints { make in
            make.trailing.leading.top.bottom.equalToSuperview()
        }
    }
    
    private func configureViews() {
        self.title = "Choose user to login"
    }
    
    //
}

extension UserListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.userCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserListItemCell.reuseIdentifier) as? UserListItemCell else {
            return UITableViewCell()
        }
        
        let item = self.viewModel.userItem(at: indexPath)
        cell.configure(item: item)
        return cell
    }
}

extension UserListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.viewModel.userItem(at: indexPath)
        let viewModel = LoginViewModel(userModel: item)
        let viewController = LoginViewController(viewModel: viewModel)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension UserListViewController: UserListViewModelToViewDelegate {
    func showUserList() {
        self.userListTableView.reloadData()
    }
}
