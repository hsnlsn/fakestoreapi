//
//  UserListViewModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

protocol UserListDataSourceProtocol: AnyObject {
    func userCount() -> Int
    func userItem(at indexPath: IndexPath) -> UserModel
}

protocol UserListViewToViewModelProtocol: AnyObject {
    var viewModelDelegate: UserListViewModelToViewDelegate? { get set }
    
    func getAllUsers()
}

protocol UserListViewModelToViewDelegate: AnyObject {
    func showUserList()
}

class UserListViewModel {
    // MARK: - Public Properties -
    
    weak var delegate: UserListViewModelToViewDelegate?
    
    //
    
    // MARK: - Private Properties -
    
    private var userList: [UserModel] = []
    
    //
}

extension UserListViewModel: UserListDataSourceProtocol {
    func userCount() -> Int {
        return self.userList.count
    }
    
    func userItem(at indexPath: IndexPath) -> UserModel {
        return self.userList[indexPath.row]
    }
}

extension UserListViewModel: UserListViewToViewModelProtocol {
    var viewModelDelegate: UserListViewModelToViewDelegate? {
        get {
            return self.delegate
        }
        set {
            self.delegate = newValue
        }
    }
    
    func getAllUsers() {
        let urlParameters: [String: Any] = ["limit": 10, "sort": "desc"]
        
        HNetworkManager.shared.sendRequest(with: UserEndpoint.getAllUsers(urlParameters: urlParameters))
        { [weak self] (result: HNetworkResult<UserModel>) in
            switch result {
            case .successArray(let userList):
                self?.userList = userList
                
                self?.delegate?.showUserList()
            case .failure(let error):
                print(error.localizedDescription)
            default:
                break
            }
        }
    }
}
