//
//  UserModel.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import ObjectMapper

struct UserModel: Mappable {
    var address: UserAddressModel?
    var id: Int?
    var email: String?
    var username: String?
    var password: String?
    var firstname: String?
    var lastname: String?
    var phone: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.address <- map["address"]
        self.id <- map["id"]
        self.email <- map["email"]
        self.username <- map["username"]
        self.password <- map["password"]
        self.firstname <- map["name.firstname"]
        self.lastname <- map["name.lastname"]
        self.phone <- map["phone"]
    }
}

struct UserAddressModel: Mappable {
    var lat: String?
    var long: String?
    var city: String?
    var street: String?
    var number: Int?
    var zipcode: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.lat <- map["geolocation.lat"]
        self.long <- map["geolocation.long"]
        self.city <- map["city"]
        self.street <- map["street"]
        self.number <- map["number"]
        self.zipcode <- map["zipcode"]
    }
}
