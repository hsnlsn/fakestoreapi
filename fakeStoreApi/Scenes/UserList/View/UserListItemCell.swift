//
//  UserListItemCell.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import UIKit

class UserListItemCell: UITableViewCell {

    static let reuseIdentifier = "UserListItemCell"

    // MARK: - Private Properties -
    
    private lazy var usernameLabel: UILabel = {
       let label = UILabel()
        return label
    }()
    
    //
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.selectionStyle = .none
        
        self.contentView.addSubview(self.usernameLabel)
        self.usernameLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
        }
    }
    
    func configure(item: UserModel) {
        self.usernameLabel.text = item.username
    }
}
