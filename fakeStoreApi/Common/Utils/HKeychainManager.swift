//
//  HKeychainManager.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import KeychainAccess

class HKeychainManager: NSObject {
    
    static let Service_Name = "FakeStoreAPI_Keychain_Service"
    static let keychain = Keychain(service: HKeychainManager.Service_Name)
    
    class func setString(item: String, forKey key: String) {
        do {
            try self.keychain.set(item, key: key)
        } catch {
            log.error("KEYCHAIN_SET_ERROR: \(error)")
        }
    }
    
    class func setData(data: Data, forKey key: String) {
        do {
            try self.keychain.set(data, key: key)
        } catch {
            log.error("KEYCHAIN_SET_ERROR: \(error)")
        }
    }
    
    class func getData(forKey key: String) -> Data? {
        do {
            return try self.keychain.getData(key)
        } catch {
            log.error("KEYCHAIN_GET_ERROR: \(error)")
            return nil
        }
    }
    
    class func getString(forKey key: String) -> String? {
        do {
            return try self.keychain.getString(key)
        } catch {
            log.error("KEYCHAIN_GET_ERROR: \(error)")
            return nil
        }
    }
    
    class func removeItem(forKey key: String) {
        do {
            try self.keychain.remove(key)
        } catch let error {
            log.error("KEYCHAIN_REMOVE_ERROR: \(error)")
        }
    }
}
