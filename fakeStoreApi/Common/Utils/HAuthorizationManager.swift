//
//  HAuthorizationManager.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

class HAuthorizationManager {
    static let shared = HAuthorizationManager()
    
    var userProfile: UserModel?
    var accessToken: AccessTokenModel?
    
    var isUserLoggedIn: Bool {
        if let token = self.accessToken?.token, !token.isEmpty {
            return true
        }
        
        return false
    }
    
    var userId: Int? {
        return self.userProfile?.id
    }
    
    func loadLoggedInUser() {
        if let jsonString = HKeychainManager.getString(forKey: HConstants.Keychain.ACCESS_TOKEN_INFO) {
            self.accessToken = AccessTokenModel(JSONString: jsonString)
        }
        
        if let jsonString = HKeychainManager.getString(forKey: HConstants.Keychain.USER_PROFILE_INFO) {
            self.userProfile = UserModel(JSONString: jsonString)
        }
    }
    
    func saveAccessToken(accessTokenModel: AccessTokenModel) {
        let accessTokenString = accessTokenModel.toJSONString() ?? ""
        HKeychainManager.setString(item: accessTokenString,
                                   forKey: HConstants.Keychain.ACCESS_TOKEN_INFO)
    }
    
    func saveUserProfile(userModel: UserModel) {
        let userProfileString = userModel.toJSONString() ?? ""
        HKeychainManager.setString(item: userProfileString,
                                   forKey: HConstants.Keychain.USER_PROFILE_INFO)
    }
    
    func removeAccessToken() {
        HKeychainManager.removeItem(forKey: HConstants.Keychain.ACCESS_TOKEN_INFO)
    }
    
    func removeUserProfile() {
        HKeychainManager.removeItem(forKey: HConstants.Keychain.USER_PROFILE_INFO)
    }
    
    static func logout() {
        // remove auth data
        HAuthorizationManager.shared.accessToken = nil
        HAuthorizationManager.shared.userProfile = nil
        HAuthorizationManager.shared.removeAccessToken()
        HAuthorizationManager.shared.removeUserProfile()
        
        // remove cache data
        URLCache.shared.removeAllCachedResponses()
    }
}
