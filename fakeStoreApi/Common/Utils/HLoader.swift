//
//  HLoader.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 3.01.2023.
//

import SVProgressHUD

extension UIApplication {
    static func resignAllResponders() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder),
                                        to: nil,
                                        from: nil,
                                        for: nil)
    }
}

class HLoader: NSObject {
    static func show() {
        UIApplication.resignAllResponders()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    static func hide() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
}
