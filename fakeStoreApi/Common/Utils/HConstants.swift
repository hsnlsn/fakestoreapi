//
//  HConstants.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation

struct HConstants {
    static var API_HOST_URL = "https://fakestoreapi.com"
    
    struct Keychain {
        static var ACCESS_TOKEN_INFO = "ACCESS_TOKEN_INFO_KEY"
        static var USER_PROFILE_INFO = "USER_PROFILE_INFO_KEY"
    }
}
