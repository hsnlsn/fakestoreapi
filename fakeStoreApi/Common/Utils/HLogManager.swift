//
//  HLogManager.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import Foundation
import SwiftyBeaver

let log = SwiftyBeaver.self

enum LogType {
    case info
    case warning
    case error
    case verbose
    case debug
}

class HLogManager {
    static let shared = HLogManager()
    var entries: [String] = []
    
    class func setup(printDocumentsPaths: Bool = false) {
        let console = ConsoleDestination()
        log.addDestination(console)
        
        if printDocumentsPaths, let documentsPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            log.verbose("documentsPathString \(documentsPathString)")
        }
    }
    
    class func logError(error: Error) {
        log.error(error.localizedDescription)
    }
}

extension SwiftyBeaver {
    static func print(type: LogType, key: String?, message: Any?) {
        switch type {
        case .info:
            log.info("\n\(key ?? ""):\n\(message ?? "")")
        case .warning:
            log.warning("\n\(key ?? ""):\n\(message ?? "")")
        case .error:
            log.error("\n\(key ?? ""):\n\(message ?? "")")
        case .verbose:
            log.verbose("\n\(key ?? ""):\n\(message ?? "")")
        case .debug:
            log.debug("\n\(key ?? ""):\n\(message ?? "")")
        }
    }
}
