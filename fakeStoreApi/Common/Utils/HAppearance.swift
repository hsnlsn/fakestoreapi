//
//  HAppearance.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 3.01.2023.
//

import UIKit

class HAppearance: NSObject {
    static func setGlobalViewAppearance() {
        // tabbar item
        HAppearance.setTabBarAppearance()
        
        // navigation bar
        HAppearance.setNavigationBarAppearance()
    }
    
    static func setTabBarAppearance() {
        let tabBarApperance = UITabBarAppearance()
        tabBarApperance.configureWithOpaqueBackground()
        tabBarApperance.backgroundColor = UIColor.white
        UITabBar.appearance().standardAppearance = tabBarApperance
        
        if #available(iOS 15.0, *) {
            UITabBar.appearance().scrollEdgeAppearance = tabBarApperance
        }
    }
    
    static func setNavigationBarAppearance() {
        if #available(iOS 15, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithOpaqueBackground()
            navigationBarAppearance.backgroundColor = UIColor.white
            UINavigationBar.appearance().standardAppearance = navigationBarAppearance
            UINavigationBar.appearance().compactAppearance = navigationBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
        }
        
        UINavigationBar.appearance().tintColor = .black
    }
}
