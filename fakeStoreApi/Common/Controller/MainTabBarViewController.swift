//
//  MainTabBarViewController.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 2.01.2023.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    private var firstTabNavigationController: UINavigationController!
    private var secondTabNavigationControoller: UINavigationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let productListViewModel = ProductListViewModel()
        let productListViewController = ProductListViewController(viewModel: productListViewModel)
        let cartViewController = CartViewController()
        
        self.firstTabNavigationController = UINavigationController.init(rootViewController: productListViewController)
        self.secondTabNavigationControoller = UINavigationController.init(rootViewController: cartViewController)
        
        self.viewControllers = [self.firstTabNavigationController,
                                self.secondTabNavigationControoller]
        
        let item1 = UITabBarItem(title: "Products",
                                 image: UIImage(systemName: "list.bullet.rectangle"),
                                 selectedImage: UIImage(systemName: "list.bullet.rectangle.fill"))
        
        let item2 = UITabBarItem(title: "Cart",
                                 image: UIImage(systemName: "cart"),
                                 selectedImage: UIImage(systemName: "cart.fill"))
        
        self.firstTabNavigationController.tabBarItem = item1
        self.secondTabNavigationControoller.tabBarItem = item2
    }
}
