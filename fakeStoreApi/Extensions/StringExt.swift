//
//  StringExt.swift
//  fakeStoreApi
//
//  Created by Hasan Asan on 3.01.2023.
//

import UIKit

extension String {
    func addingParagraphStyle(lineSpacing: CGFloat) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineSpacing

        let range = NSMakeRange(0, attributedText.length)
        attributedText.addAttributes([.paragraphStyle: paragraphStyle], range: range)
        return attributedText
    }
}
