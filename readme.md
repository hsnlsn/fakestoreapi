# fakeStoreApi iOS App

Supports: iOS 15.2 and above

Swift: 5.x and above

Architecture Pattern: MVVM ([see more on DuckDuckGo](https://duckduckgo.com/?q=mvvm+ios))

## Branches:

* master - stable app releases
* develop - development branch, merge your feature branches here

## Dependencies:

The project is using cocoapods for managing external libraries and a Gemfile for managing the cocoapods version.

Get CocoaPods

```
sudo gem install cocoapods
```

Then install the pods

```
pod install
```

### Core Dependencies

* Alamofire - HTTP networking library
* ObjectMapper - Convert model objects (classes and structs) to and from JSON.

## Project structure:

* Resources - fonts, strings, images, generated files etc.
* Scenes - contains app scenes (UI + Code)
* Network - Endpoint configurations and network requests
* Common -  base models, base views and utility classes